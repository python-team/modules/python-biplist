python-biplist (1.0.3-5) unstable; urgency=medium

  * Team upload.
  * Switch from nose to pytest (closes: #1018465).

 -- Colin Watson <cjwatson@debian.org>  Sun, 11 Dec 2022 21:48:14 +0000

python-biplist (1.0.3-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 26 May 2022 17:01:30 +0100

python-biplist (1.0.3-3) unstable; urgency=medium

  * Team Upload.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set upstream metadata fields: Repository.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Stefano Rivera ]
  * Patch: Python 3.9 support (Closes: #973082).
  * Bump watch file version to 4.
  * Bump Standards-Version to 4.5.1, no changes needed.
  * Declare Rules-Requires-Root: no.
  * Remove leading ./ from debian/copyright Files.
  * Extend upstream metadata.

 -- Stefano Rivera <stefanor@debian.org>  Mon, 01 Feb 2021 18:21:53 -0700

python-biplist (1.0.3-2) unstable; urgency=medium

  * Team upload.
  * d/control: Remove ancient X-Python-Version field
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support (Closes: #937609).

 -- Ondřej Nový <onovy@debian.org>  Tue, 10 Dec 2019 15:08:35 +0100

python-biplist (1.0.3-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.1.3 no changes

 -- Hans-Christoph Steiner <hans@eds.org>  Thu, 22 Feb 2018 11:18:22 +0100

python-biplist (1.0.2-1) unstable; urgency=medium

  * New upstream release
  * fix tests on 32-bit arches (Closes: #860656)

 -- Hans-Christoph Steiner <hans@eds.org>  Mon, 15 May 2017 11:12:50 +0200

python-biplist (1.0.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * debian/control:
    - Depend on debhelper (>=9) instead of (>=8).
    - Bump Standards-Version to 3.9.8.
    - Build Python 3 binary package (add all required python3-* Build-Deps).
    - Remove python-six from the Build-Dependency field: No longer needed.
    - Update Vcs-* fields to use secure protocols (HTTPS).
  * debian/copyright:
    - Fix typos: 'bsd' -> 'BSD-3-Clause', 'public domain' -> 'public-domain'.
  * Bump compatibility level to 9.

 -- Hugo Lefeuvre <hle@debian.org>  Wed, 14 Sep 2016 15:30:45 +0200

python-biplist (0.7-1) unstable; urgency=low

  * Initial release (Closes: #729510)

 -- Hans-Christoph Steiner <hans@eds.org>  Mon, 14 Jul 2014 20:13:00 -0400
